def factors(number):
    # ==============
    list_range = list(range(2, number))
    facts=[]
    for num in list_range:
        if number % num == 0:
            facts.append(num)
    return facts



    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
