def vowel_swapper(string):
    # ==============
    x = string.replace("a", "4")
    x = x.replace("A", "4")
    x = x.replace("e", "3")
    x = x.replace("E", "3")
    x = x.replace("i", "!")
    x = x.replace("I", "!")
    x = x.replace("o", "ooo")
    x = x.replace("O", "000")
    x = x.replace("u", "|_|")
    x = x.replace("U", "|_|")

    return x
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console